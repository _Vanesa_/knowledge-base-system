package cucei.udg.knowledgebasesystem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.net.Uri;
import android.content.Intent;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Main extends AppCompatActivity
{
    TextView rulesView, question, result;
    Button yes, no, changeFile;
    CheckBox forward, backward;
    String fileString;
    cucei.udg.knowledgebasesystem.FileReader fReader;
    Deserializer deserializer;
    KnowledgeBase kb;
    InferenceEngine ie;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        rulesView = findViewById(R.id.fileContent);
        question = findViewById(R.id.question);
        yes = findViewById(R.id.yesBtn);
        no = findViewById(R.id.noBtn);
        changeFile = findViewById(R.id.chooseFileBtn);
        result = findViewById(R.id.result);
        forward = findViewById(R.id.fowardCkbx);
        backward = findViewById(R.id.backwarcCkbx);

        forward.setChecked(true);


        fReader = new cucei.udg.knowledgebasesystem.FileReader();

        //Cargar archivo Zoo
        try
        {
            load(getFileFromAssets());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public BufferedReader getFileFromAssets() throws IOException
    {
        /*Se carga el archivo zoo.xml que sera el por defecto del programa*/
        return new BufferedReader(new InputStreamReader(getAssets().open("zoo.xml")));
    }

    public void openExplorer(View view)
    {
        /* Este metodo abrira el explorador de archivos para que nuevas reglas se puedan escoger ***/
        /* Se espera el archivo recogido este en .xml ***/
        Intent intent = new Intent().setType("application/xml").setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        /*Este metodo correra al ser cerrado el exploraodr de archivos  **/
        super.onActivityResult(requestCode, resultCode, data);

        /* Al llegar a esta parte se revisa si en efecto un nuevo archivo de reglas se ha escogido
            de no ser asi no debera pasar nada y el programa continua con zoo.xml **/
        if (requestCode == 123 && resultCode == RESULT_OK)
        {
            Uri rulesFile = data.getData(); /* El URI del nuevo archivo */

            /*La verificacion de extension no funciona cuando se abre un explorador de archivo externo
               al del telefono asi que se vuelve a verificar*/
            assert rulesFile != null;
            if (FileReader.mimeTypeIsValid(getApplicationContext(), rulesFile))
            {
                //Si el archivo es valido solo se muestra el nombre que se le ha dado al archivo en el sistema
                //En esta parte se extrae ese nombre
                String path = Uri.fromFile(new File(rulesFile.getPath())).toString();
                String filename = path.substring(path.lastIndexOf("/")+1);

                changeFile.setText(filename);

                //Reemplazar a zoo.xml
                try
                {
                    InputStream inStream = getContentResolver().openInputStream(rulesFile);
                    assert inStream != null;
                    BufferedReader newFile = new BufferedReader(new InputStreamReader(inStream));
                    load(newFile);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                stopProcess("Los archivos de reglas deben ser .xml");
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void load(BufferedReader newFile) throws IOException
    {
        result.setTextColor(Color.GRAY);
        result.setText("Esperando resultados ...");

        fileString = fReader.readFile(newFile);
        deserializer = new Deserializer(fileString);
        kb = new KnowledgeBase(deserializer.deserialize());

        if (!kb.isEmpty())
        {
            ie = new InferenceEngine(kb);
            yes.setEnabled(true);
            no.setEnabled(true);
            backward.setEnabled(true);
            forward.setEnabled(true);

            //"Jugar" con el nuevo archivo
            if (backward.isChecked())//Si se escojio el modo encadenar hacia atras
            {
                ie.backwardChaining();
            }
            rulesView.setText(String.valueOf(ie.getKbFoward()));
            ie.forwardchaining(question);
        }
        else
        {
            stopProcess("Formato no soportado");
        }
    }

    @SuppressLint("SetTextI18n")
    void stopProcess(String message)
    {
        changeFile.setText(message);
        question.setText("Esperando reglas...");
        yes.setEnabled(false);
        no.setEnabled(false);
        backward.setEnabled(false);
        forward.setEnabled(false);
        result.setText("Esperando resultados ...");

        if (!message.equals("Selecciona nuevas reglas"))
            rulesView.setText("");
    }

    public void setUserResponse(View view)
    {
        if (view == yes)
            ie.setCurrentAtomState(true);
        else
            ie.setCurrentAtomState(false);

        ArrayList response = ie.forwardchaining(question);

        checkForFinale(response);
    }

    void checkForFinale(ArrayList response)
    {
        if (response.size() > 0)
        {
            String responseString = String.valueOf(response);
            int x = (response.get(0) == "Failed" ? 1 : 3);
            responseString = responseString.substring(x, responseString.length() - 1);

            if (responseString.equals("Failed"))
            {
                if (backward.isChecked())
                {
                    Log.d("PBackward", "response" + responseString);
                    ie.clearSuccess();
                    if (!ie.backwardChaining())
                    {
                        Log.d("PBackward", "False");
                        finishProcess(responseString);
                    }
                    else
                    {
                        Log.d("PBackward", "True");
                        rulesView.setText(String.valueOf(ie.getKbFoward()));
                        ArrayList _response = ie.forwardchaining(question);

                        checkForFinale(_response);
                    }
                }
                else
                {
                    finishProcess(responseString);
                }
            }
            else
            {
                finishProcess(responseString);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    void finishProcess(String response)
    {
        if (response.equals("Failed"))
            result.setTextColor(ContextCompat.getColor(context, R.color.colorError));
        else
            result.setTextColor(ContextCompat.getColor(context, R.color.inferenceSuccess));

        question.setText("es");
        stopProcess("Selecciona nuevas reglas");
        result.setText("Conclusión " + response);
    }

    void beginBackward()
    {
        Log.d("Play", " \n \n **************************** Backward Chaning ****************************");
        ie.getKb().setObjectivesAsUnused();
        ie.getKb().unMetRules();
        ie.getKb().setRulesAsUnused();
        ie = new InferenceEngine(ie.getKb());
        ie.setPlayingBackaward(true);
        ie.backwardChaining();
        ie.forwardchaining(question);
        rulesView.setText(String.valueOf(ie.getKbFoward()));
    }

    void beginForward()
    {
        Log.d("Play", " \n \n **************************** Forward Chaning ****************************");
        ie = new InferenceEngine(ie.getKb());
        ie.forwardchaining(question);
        rulesView.setText(ie.getKb().toString());
    }

    public void setMode(View view)
    {
        if (view == forward)
        {
            if(forward.isChecked())
            {
                backward.setChecked(false);
                beginForward();
            }
            else
            {
                backward.setChecked(true);
                beginBackward();
            }
        }
        else
        {
            if(backward.isChecked())
            {
                forward.setChecked(false);
                beginBackward();
            }
            else
            {
                forward.setChecked(true);
                beginForward();
            }
        }
    }
}
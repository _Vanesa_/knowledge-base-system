package cucei.udg.knowledgebasesystem;

import java.util.ArrayList;

public class KnowledgeBase
{
    private ArrayList<Rule> kb;

    KnowledgeBase(ArrayList<Rule> kb)
    {
        this.kb = (ArrayList<Rule>) kb.clone();
    }

    boolean isEmpty()
    {
        return kb == null;
    }

    @Override
    public String toString()
    {
        StringBuilder kbDescription = new StringBuilder();

        for(Object rule : kb)
        {
            kbDescription.append(rule).append("\n");
        }
        return kbDescription.toString();
    }

    public ArrayList<Rule> getKb()
    {
        return kb;
    }

    public void setObjectivesAsUnused()
    {
        for (Rule rule: kb)
        {
            if(rule.isObjective() && rule.isUsedAsObjective())
                rule.setUsedAsObjective(false);
        }
    }

    void setRulesAsUnused()
    {
        for (Rule rule: kb)
        {
            if(rule.isUsed())
                rule.setUsed(false);
        }
    }

    void unMetRules()
    {
        for (Rule rule: kb)
        {
            if(rule.isMet())
                rule.setMet(false);
        }
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}

package cucei.udg.knowledgebasesystem;

import android.util.Log;

import java.util.ArrayList;

public class Memory
{
    private ArrayList<Atom> trueAtoms, falseAtoms;

    Memory()
    {
        trueAtoms = new ArrayList<>();
        falseAtoms = new ArrayList<>();
    }

    public void addToMemory(Atom atom) throws DuplicatedAtom
    {
        if (!atomIsInMEmory(atom))
        {
            Log.d("PForward", "No Exception");
            if (atom.getState())
                trueAtoms.add(atom);
            else
                falseAtoms.add(atom);
        }
        else
        {
            Log.d("PForward", "Exception");
            throw new DuplicatedAtom(atom.getContent());
        }
    }
    public boolean atomIsIn(Atom atom)
    {
        return trueAtoms.contains(atom) || !falseAtoms.contains(atom);
    }


    public boolean atomIsInMEmory(Atom atom)
    {
        Atom aux = new Atom(atom.getContent(), atom.getState(), atom.isObjective());
        aux.setState(!aux.getState());

        return (trueAtoms.contains (atom)||falseAtoms.contains (atom)||
                trueAtoms.contains (aux)||falseAtoms.contains (aux));
    }

    public void clearMemory()
    {
        trueAtoms.clear();
        falseAtoms.clear();
    }

    public Atom getFromMemory(Atom toFind)
    {
        int iT = trueAtoms.indexOf(toFind);
        int iF = falseAtoms.indexOf(toFind);
        if (iT > -1)
            return trueAtoms.get(iT);
        if (iF > -1)
            return falseAtoms.get(iF);
        return null;
    }

    @Override
    public String toString()
    {
        StringBuilder memory= new StringBuilder("\n True ");
        for(Object condition : trueAtoms)
        {
            memory.append(condition).append(" ");
        }
        memory.append("\n False ");

        for(Object conclusion : falseAtoms)
        {
            memory.append(conclusion).append(" ");
        }
        return memory.toString();

    }
}

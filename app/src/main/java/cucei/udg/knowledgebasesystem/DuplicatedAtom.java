package cucei.udg.knowledgebasesystem;

public class DuplicatedAtom extends Exception
{
    DuplicatedAtom(String content)
    {
        super("*********Atomo Duplicado: " + content + " *********");
    }

    @Override
    public String toString()
    {
        return super.toString();
    }
}


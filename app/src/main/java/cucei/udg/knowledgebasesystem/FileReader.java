package cucei.udg.knowledgebasesystem;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

public class FileReader
{
    public String readFile(BufferedReader r) throws IOException
    {
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null)
        {
            total.append(line).append('\n');
        }
        return String.valueOf(total);
    }

    public static boolean mimeTypeIsValid(Context context, Uri uri)
    {
        String extension;
        boolean valid = false;

        //Checar le formato de URI
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT))
        {
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        }
        else
        {
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }

        /* Si se abrio el archivo con el explorador del telefono la extension sera null
          si no la extension si tendra contenido*/
        if (extension == null || extension.equals("xml"))
        {
            valid = true;
        }

        return valid;
    }

}

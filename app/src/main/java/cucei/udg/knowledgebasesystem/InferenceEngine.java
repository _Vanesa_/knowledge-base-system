package cucei.udg.knowledgebasesystem;


import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class InferenceEngine
{
    private KnowledgeBase kbFoward, kb;
    private Memory workMemory;
    private Atom currentAtom;
    private Rule currentRule, currentObjective;
    private int iRule, iatom, iobjective;
    private ArrayList<Object> success;
    private ArrayList<Rule> objectives;
    private boolean playingBackaward;

    InferenceEngine(KnowledgeBase kb)
    {
        this.kb = new KnowledgeBase(kb.getKb());
        kbFoward = new KnowledgeBase(kb.getKb());
        currentAtom = null;
        currentRule = null;
        currentObjective = null;
        iatom = iRule = iobjective = 0;
        success = new ArrayList<>();
        objectives = new ArrayList<>();
        workMemory = new Memory();
        playingBackaward = false;
        getNextRule();//para recoger la primera regla
    }

    public KnowledgeBase getKb()
    {
        return kb;
    }

    public void clearSuccess()
    {
        success.clear();
    }

    public void setKbFoward(KnowledgeBase kbFoward)
    {
        this.kbFoward = kbFoward;
    }

    public KnowledgeBase getKbFoward()
    {
        return kbFoward;
    }

    public Memory getWorkMemory()
    {
        return workMemory;
    }

    public void setIatom(int iatom)
    {
        this.iatom = iatom;
    }

    public void setiRule(int iRule)
    {
        this.iRule = iRule;
    }

    public void setPlayingBackaward(boolean playingBackaward)
    {
        this.playingBackaward = playingBackaward;
    }

    /**************************** Forward Chaning ****************************/

    @SuppressLint("SetTextI18n")
    public ArrayList<Object> forwardchaining(TextView q)
    {
        //obtener el siguiente atomo en su parte condicion
        getNextAtom();
        if (currentAtom != null)
        {
            Log.d("PForward", "-> " + currentAtom.getContent());
            q.setText("¿Se cumple: " + currentAtom.getContent() + "?");
        }

        return success;
    }

    private void getNextAtom()
    {
        if (iatom < currentRule.getConditions().size())
        {
            if((currentRule.getConditions().get(iatom) instanceof Atom) &&
                    (!workMemory.atomIsInMEmory((Atom)currentRule.getConditions().get(iatom))))
            {
                currentAtom = (Atom) currentRule.getConditions().get(iatom);
                iatom++;
                Log.d("PForward", "\t Current Atom ->" + currentAtom);
            }
            else
            {
                iatom++;
                getNextAtom();
            }
        }
        else
        {
            iatom = 0;
            currentAtom = null;
            //la regla que se acaba de completar debe de ser evaluada primero
            analizeRuleConditions();

            if (currentRule.isMet())
            {
                Log.d("PForward", "Rule met");
                //si la regla se cumple se analizaran sus partes conclusion
                analizeRuleConclusions();
                //si la regla contiene un atomo que es objetivo este sera el resultado de ejecucion
                if (currentRule.isObjective())
                    success = currentRule.getConclusions();
            }
            else
            {
                //Si se est jugando con backward chaining y una regla no se cumple ya no se llegara
                //al objetivo por lo que es mejor detener el proceso
                if(playingBackaward)
                {
                    Log.d("PBackward", "Rule not met");
                    success.add("Failed");
                }
                else
                {
                    Log.d("PForward", "Rule not met");
                }
            }

            //mientras no se haya llegado a una conclusion
            if (success.size() == 0)
            {
                getNextRule();
                if (currentRule != null)
                {
                    getNextAtom();
                }
                else
                {
                    // si ya no hay reglas no se ha llegado a un resultado de ejecucion
                    success.add("Failed");
                }
            }
        }
    }

    private void getNextRule()
    {
        //cambiando de regla
        if (iRule < kbFoward.getKb().size())
        {
            currentRule = kbFoward.getKb().get(iRule);
            iRule++;
            Log.d("PForward", "Current Rule " + currentRule.toString());
        }
        else
        {
            currentRule = null;
            iRule = 0;
        }
    }

    private void analizeRuleConditions()
    {
        Stack<Boolean> boolStack = new Stack<>();
        Atom tmp;
        boolean comparationA, comparationB;

        for (Object condition: currentRule.getConditions())
        {
            if (condition instanceof Atom)
            {
                tmp=(Atom)condition;
                comparationA = compareTruths(tmp, workMemory.getFromMemory(tmp));
                boolStack.push(comparationA);
            }
            else if (condition.equals("~"))
            {
                comparationA = boolStack.pop();
                comparationA =! comparationA;
                boolStack.push(comparationA);
            }
            else if (condition.equals("&"))
            {
                comparationA = boolStack.pop();
                comparationB = boolStack.pop();
                boolStack.push(comparationA && comparationB);
            }
            else if (condition.equals("|"))
            {
                comparationA = boolStack.pop();
                comparationB = boolStack.pop();
                boolStack.push(comparationA || comparationB);
            }
        }
        currentRule.setMet(boolStack.pop());
    }

    private void analizeRuleConclusions()
    {
        Atom tmp = null;
        ArrayList<Atom> tmpArrayAtoms = new ArrayList<>();

        currentRule.setMet(true);

        for(Object conclusion : currentRule.getConclusions())
        {
            if (conclusion instanceof Atom)
            {
                tmp = new Atom(((Atom) conclusion).getContent(), ((Atom) conclusion).getState(),
                        ((Atom) conclusion).isObjective());

                tmpArrayAtoms.add(tmp);
            }
            else if (conclusion.equals("~"))
            {
                assert tmp != null;
                tmp.setState(!tmp.getState());
            }
        }
        for(Object atom : tmpArrayAtoms)
        {
            try
            {
                workMemory.addToMemory((Atom)atom);
            }
            catch(DuplicatedAtom duplicatedAtom)
            {
                duplicatedAtom.printStackTrace();
            }
        }
    }

    public void setCurrentAtomState(boolean state)
    {
        currentAtom.setState(state);
        try
        {
            workMemory.addToMemory(currentAtom);

            Log.d("PForwardM", "Current Memory " + workMemory.toString());
        }
        catch (DuplicatedAtom duplicatedAtom)
        {
            duplicatedAtom.printStackTrace();
        }
    }

    private boolean compareTruths(Atom a, Atom b)
    {
        if (b != null)
            return a.getState() && b.getState();

        return false;
    }

    /**************************** Backward Chaning ****************************/

    public boolean backwardChaining()
    {
        //Si esta es la primera vez que se corre
        if (objectives.size() == 0)
            setObjectives();

        kb.unMetRules();
        kb.setRulesAsUnused();
        KnowledgeBase tmpKB = new KnowledgeBase(kb.getKb());

        //se obtiene un objetivo nuevo
        getNextObjective();

        if (currentObjective != null)
        {
            ArrayList<Atom> lookedFor = new ArrayList<>();
            ArrayList<Atom> statesChanged = new ArrayList<>();
            ArrayList<Rule> newKB = new ArrayList<>();


            //se le cambia el estado segun su negacion
            setStates(statesChanged, currentObjective.getConclusions());
            //se añade el objetivo a los atomos que se deben de cumplir
            addToLookedFor(lookedFor, statesChanged);
            //Se debe de crear una base de conocimiento nueva que pueda ser utilizada
            //por encadenamiento hacia delante
            //Para lograr esto se deben de encontrar todas las reglas que llegan a
            //el objetivo seleccionado
            boolean usefulRulesFound = true;
            do
            {
                System.out.println();
                //Por si ya no se encuentran reglas que se puedan usar
                usefulRulesFound = false;
                for (Rule rule : tmpKB.getKb())
                {
                    boolean x = isLookedFor(rule, lookedFor);
                    if ((!rule.isUsed()) && (x))
                    {
                        //Aun hay reglas que nos llevan por ese camino
                        usefulRulesFound = true;
                        //para o volver a usar esta regla
                        rule.setUsed(true);
                        //se le cambia el estado segun su negacion
                        statesChanged.clear();
                        setStates(statesChanged, rule.getConditions());
                        //se añade el objetivo a los atomos que se deben de cumplir
                        addToLookedFor(lookedFor, statesChanged);
                        //se va creando la base nueva
                        newKB.add(0, rule);
                    }
                }
            }while(usefulRulesFound);

            //Se cambia la base de conocimiento que usa encadenamiento hacia delante
            kbFoward = new KnowledgeBase(newKB);
            iatom = iRule = 0;
            //se inicializa la primera regla
            getNextRule();
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean isLookedFor(Rule rule, ArrayList<Atom> lookedFor)
    {
        ArrayList<Atom> atomsToEvaluate = new ArrayList<>();
        //Se le cambia los estados a la conclusion segun su negacion
        setStates(atomsToEvaluate, rule.getConclusions());
        //Se revisa si este atomo se debe cumplir
        for(Atom atom : atomsToEvaluate)
        {
            if (lookedFor.contains(atom))
                return true;
        }
        return false;
    }

    private void addToLookedFor(ArrayList<Atom> oldConclusions, ArrayList<Atom> newConclusions)
    {
        oldConclusions.addAll(newConclusions);
    }

    private void setStates(ArrayList<Atom> setAtoms, ArrayList unsetAtoms)
    {
        Atom newAtom = null;
        for(Object conclusion : unsetAtoms)
        {
            if (conclusion instanceof Atom)
            {
                newAtom = new Atom(((Atom) conclusion).getContent(),
                        ((Atom) conclusion).getState(),
                        ((Atom) conclusion).isObjective());
                setAtoms.add(newAtom);
            }
            else if (conclusion.equals("~"))
            {
                newAtom.setState(!newAtom.getState());
            }
        }
    }

    private void setObjectives()
    {
        //Se crea una lista con todos los objetivos que hay en la base de conocimiento
        for(Rule rule : kb.getKb())
        {
            if (rule.isObjective())
            {
                objectives.add(rule);
            }
        }
    }

    private void getNextObjective()
    {
        //Por cada objetivo que se tiene se van esocjiendo aleatoriamente
        Random rand = new Random();
        iobjective = -1;
        int i = 0;
        currentObjective = null;

        while ((i < objectives.size()) && (currentObjective == null))
        {
            iobjective = rand.nextInt(objectives.size());
            if(!objectives.get(iobjective).isUsedAsObjective())
            {
                currentObjective = objectives.get(iobjective);
                currentObjective.setUsedAsObjective(true);
                Log.d("PBackward", "Current Objective " + currentObjective.toString());
            }
            else
            {
                iobjective = -1;
            }
            i++;
        }
    }
}
